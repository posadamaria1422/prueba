package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainMenuActivity extends AppCompatActivity {

    BottomNavigationView bottom_navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_main);

        bottom_navigation = findViewById(R.id.menu_home);
        bottom_navigation.setOnClickListener(v -> onMenu_homeClick());

        bottom_navigation = findViewById(R.id.menu_add);
        bottom_navigation.setOnClickListener(v -> onMenu_addClick());

        bottom_navigation = findViewById(R.id.menu_profile);
        bottom_navigation.setOnClickListener(v -> onMenu_profileClick());
    }

    private void onMenu_homeClick(){
        startActivity (new Intent(MainMenuActivity.this, OptionsActivity.class));
    }

    private void onMenu_addClick(){
        startActivity (new Intent(MainMenuActivity.this, PetitionActivity.class));

    }

    private void onMenu_profileClick(){
        startActivity (new Intent(MainMenuActivity.this, LoginActivity.class));

    }
}