package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;

import com.example.myapplication.R;


public class HomeActivity extends AppCompatActivity {

    AppCompatButton btnLoginH;
    AppCompatButton btnRegisterH;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        btnRegisterH = findViewById(R.id.btn_register_h);
        btnRegisterH.setOnClickListener(v -> onRegisterHClick());

        btnLoginH = findViewById(R.id.btn_login_h);
        btnLoginH.setOnClickListener(v -> onLoginHClick());
    }

    private void onRegisterHClick() {
        Intent intent = new Intent(HomeActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    private void onLoginHClick() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
    }

}