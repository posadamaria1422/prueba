package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.mvp.RegisterMVP;
import com.example.myapplication.presenter.RegisterPresenter;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class RegisterActivity extends AppCompatActivity implements RegisterMVP.View {

    TextInputLayout tilEmailR;
    TextInputEditText etEmailR;
    TextInputLayout tilPasswordR;
    TextInputEditText etPasswordR;
    TextInputLayout tilConfirmP;
    TextInputEditText etConfirmP;

    LinearProgressIndicator pbWait;
    AppCompatButton btnRegisterR;

    private RegisterMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        presenter = new RegisterPresenter(this);

        initUI();
    }

    private void initUI() {
        tilEmailR = findViewById(R.id.til_email_r);
        etEmailR = findViewById(R.id.et_email_r);

        tilPasswordR = findViewById(R.id.til_password_r);
        etPasswordR = findViewById(R.id.et_password_r);

        tilConfirmP = findViewById(R.id.til_confirm_p);
        etConfirmP = findViewById(R.id.et_confirm_p);

        pbWait = findViewById(R.id.pb_wait);

        btnRegisterR = findViewById(R.id.btn_register_r);
        btnRegisterR.setOnClickListener(v -> presenter.onRegister_rClick());
    }

    @Override
    public Activity getActivity() {
        return RegisterActivity.this;
    }

    @Override
    public RegisterMVP.RegisterInfo getRegisterInfo() {
        return new RegisterMVP.RegisterInfo(
                etEmailR.getText().toString().trim(),
                etPasswordR.getText().toString().trim(),
                etConfirmP.getText().toString().trim());
    }

    @Override
    public void showEmailError(String error) {
        tilEmailR.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        tilPasswordR.setError(error);
    }

    @Override
    public void showConfirmpError(String error) {
        tilConfirmP.setError(error);
    }

    @Override
    public void showLoginActivity() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(RegisterActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnRegisterR.setEnabled(false);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnRegisterR.setEnabled(true);
    }
}