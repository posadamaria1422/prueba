package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.myapplication.R;
import com.example.myapplication.mvp.LoginMVP;
import com.example.myapplication.presenter.LoginPresenter;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


public class LoginActivity extends AppCompatActivity implements LoginMVP.View {

    TextInputLayout tilEmailL;
    TextInputEditText etEmailL;
    TextInputLayout tilPasswordL;
    TextInputEditText etPasswordL;

    LinearProgressIndicator pbWait;

    AppCompatButton btnLoginL;
    AppCompatButton btnRegisterL;
    AppCompatImageButton btnGoogle;
    AppCompatImageButton btnFacebook;

    private LoginMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        presenter = new LoginPresenter(this);

        initUI();
    }

    private void initUI() {
        tilEmailL = findViewById(R.id.til_email_l);
        etEmailL= findViewById(R.id.et_email_l);

        tilPasswordL = findViewById(R.id.til_password_l);
        etPasswordL = findViewById(R.id.et_password_l);

        pbWait = findViewById(R.id.pb_wait);

        btnLoginL = findViewById(R.id.btn_login_l);
        btnLoginL.setOnClickListener(v -> presenter.onLogin_lClick());

        btnRegisterL = findViewById(R.id.btn_register_l);
        btnRegisterL.setOnClickListener(v -> presenter.onRegister_lClick());

        btnGoogle = findViewById(R.id.btn_google);
        btnGoogle.setOnClickListener(v -> presenter.onGoogleClick());

        btnFacebook = findViewById(R.id.btn_facebook);
        btnFacebook.setOnClickListener(v -> presenter.onFacebookClick());
    }


    @Override
    public Activity getActivity() {
        return LoginActivity.this;
    }

    @Override
    public LoginMVP.LoginInfo getLoginInfo() {
        return new LoginMVP.LoginInfo(
                etEmailL.getText().toString().trim(),
                etPasswordL.getText().toString().trim());
    }

    @Override
    public void showPasswordError(String error) {
        tilPasswordL.setError(error);
    }

    @Override
    public void showEmailError(String error) {
        tilEmailL.setError(error);
    }

    @Override
    public void showOptionsActivity() {
        Intent intent = new Intent(LoginActivity.this, OptionsActivity.class);
        startActivity(intent);
    }

    @Override
    public void showRegisterActivity() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void showGeneralError(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        pbWait.setVisibility(View.VISIBLE);
        btnLoginL.setEnabled(false);
        btnRegisterL.setEnabled(false);
        btnFacebook.setEnabled(false);
        btnGoogle.setEnabled(false);
    }

    @Override
    public void hideProgressBar() {
        pbWait.setVisibility(View.GONE);
        btnLoginL.setEnabled(true);
        btnRegisterL.setEnabled(true);
        btnFacebook.setEnabled(true);
        btnGoogle.setEnabled(true);
    }

    @Override
    public void showAccountError(String error) {
            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
    }
}