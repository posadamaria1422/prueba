package com.example.myapplication.mvp;

import android.app.Activity;

public interface RegisterMVP {
    interface Model {

        void validateCredentials(String email, String password,String password2,
                                 LoginMVP.Model.ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {
        void onRegister_rClick();
    }

    interface View {
        Activity getActivity();

        RegisterMVP.RegisterInfo getRegisterInfo();

        void showPasswordError(String error);

        void showConfirmpError(String error);

        void showEmailError(String error);

        void showLoginActivity();

        void showGeneralError(String error);

        void showProgressBar();

        void hideProgressBar();
    }

    class RegisterInfo {
        private String email;
        private String password;
        private String password2;

        public RegisterInfo(String email, String password, String password2) {
            this.email = email;
            this.password = password;
            this.password2 = password2;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }

        public String getConfirmp() {
            return password2;
        }

    }
}

