package com.example.myapplication.mvp;

import android.app.Activity;

public interface LoginMVP {
    interface Model {

        void validateCredentials(String email, String password,
                                 ValidateCredentialsCallback callback);

        interface ValidateCredentialsCallback {
            void onSuccess();

            void onFailure();
        }
    }

    interface Presenter {
        void onLogin_lClick();

        void onRegister_lClick();

        void onFacebookClick();

        void onGoogleClick();
    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();

        void showPasswordError(String error);

        void showEmailError(String error);

        void showOptionsActivity();

        void showRegisterActivity();

        void showGeneralError(String error);

        void showProgressBar();

        void hideProgressBar();

        void showAccountError(String error);
    }

    class LoginInfo {
        private String email;
        private String password;

        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public String getEmail() {
            return email;
        }

        public String getPassword() {
            return password;
        }
    }
}