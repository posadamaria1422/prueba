package com.example.myapplication.presenter;

import com.example.myapplication.model.LoginInteractor;
import com.example.myapplication.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor();
    }

    @Override
    public void onLogin_lClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Validación de datos
        view.showEmailError("");
        view.showPasswordError("");

        if (loginInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es requerido");
            error = true;
        } else if (!isEmailValid(loginInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (loginInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es requerida");
            error = true;
        } else if (!isPasswordValid(loginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> {
                model.validateCredentials(loginInfo.getEmail(), loginInfo.getPassword(),
                        new LoginMVP.Model.ValidateCredentialsCallback() {
                            @Override
                            public void onSuccess() {
                                view.getActivity().runOnUiThread(() -> {
                                    view.hideProgressBar();
                                    view.showOptionsActivity();
                                });
                            }

                            @Override
                            public void onFailure() {
                                view.getActivity().runOnUiThread(() -> {
                                    view.hideProgressBar();
                                    view.showGeneralError("Credenciales inválidas");
                                });
                            }
                        });
            }).start();
        }
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com") || email.endsWith(".co");
    }

    @Override
    public void onRegister_lClick() {
        view.showRegisterActivity();
    }

    @Override
    public void onFacebookClick() {
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        view.showAccountError("");

        if (loginInfo.getEmail().isEmpty() && loginInfo.getPassword().isEmpty()) {
            view.showOptionsActivity();
        } else {
            view.showAccountError("Los campos deben estar vacios para acceder con Facebook");
        }
    }

    @Override
    public void onGoogleClick() {
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        view.showAccountError("");

        if (loginInfo.getEmail().isEmpty() && loginInfo.getPassword().isEmpty()) {
            view.showOptionsActivity();
        } else {
            view.showAccountError("Los campos deben estar vacios para acceder con Google");
        }
    }
}