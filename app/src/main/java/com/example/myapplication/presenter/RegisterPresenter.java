package com.example.myapplication.presenter;

import com.example.myapplication.model.RegisterInteractor;
import com.example.myapplication.mvp.LoginMVP;
import com.example.myapplication.mvp.RegisterMVP;

public class RegisterPresenter implements RegisterMVP.Presenter {

    private RegisterMVP.View view;
    private RegisterMVP.Model model;

    public RegisterPresenter(RegisterMVP.View view) {
        this.view = view;
        this.model = new RegisterInteractor();
    }

    @Override
    public void onRegister_rClick() {
        boolean error = false;
        RegisterMVP.RegisterInfo registerInfo = view.getRegisterInfo();

        //Validación de datos
        view.showEmailError("");
        view.showPasswordError("");
        view.showConfirmpError("");

        if (registerInfo.getEmail().isEmpty()) {
            view.showEmailError("Correo electrónico es requerido");
            error = true;
        } else if (!isEmailValid(registerInfo.getEmail())) {
            view.showEmailError("Correo electrónico no es válido");
            error = true;
        }

        if (registerInfo.getPassword().isEmpty()) {
            view.showPasswordError("Contraseña es requerida");
            error = true;
        } else if (!isPasswordValid(registerInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple criterios de seguridad");
            error = true;
        }

        if (registerInfo.getConfirmp().isEmpty()) {
            view.showConfirmpError("Cofirmación requerida");
            error = true;
        } else if (!isComfirmpValid(registerInfo.getPassword(), registerInfo.getConfirmp())) {
            view.showConfirmpError("Las contraseñas no coinsiden");
            error = true;
        }

        view.showLoginActivity();
    }

    private boolean isComfirmpValid(String password2, String password) {
        return password != password2;
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@")
                && email.endsWith(".com") || email.endsWith(".co");
    }
}
